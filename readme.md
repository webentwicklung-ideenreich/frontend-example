# Installation
To install the project run *npm install* and *npm install -g json-server*.

# Build Assets
To build assets run *npm run dev*. Also start json-server with *npm run json-server*.

# Todos (After every todo a git commit should be done)
- Add functionalities to load, add and remove todos from the server. 
- Add a feature to mark a todo as DONE. For this you have to adjust the data structure in the db.json File and in the App component.
- Add a feature to show how many todos are DONE and how many todos are incomplete. (take a look at Vues computed property).
- Change the current layout so that it looks like the mockup. (you can find the mockup pdf and additional information in the mockup directory)

# Helpful resources
https://vuejs.org/v2/guide/
https://github.com/typicode/json-server
https://github.com/axios/axios
http://lesscss.org/usage/

# Colors
green:		#94ac67
turquoise: 	#accfcc
light:		#deeceb
grey:		#666666
lightgrey: 	#b3b3b3

# Icons
https://glyphsearch.com/?library=font-awesome
<i class="fa fa-trash"></i> 
<i class="fa fa-plus-circle"></i>
<i class="fa fa-check-square-o"></i>
<i class="fa fa-check"></i>

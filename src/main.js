// Vue
import Vue from 'vue';
Vue.config.productionTip = false;

// Axios
import axios from 'axios';
window.axios = axios;
axios.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        // error 400 - token ungültig
        if (error.response && error.response.status === 400) {
            Auth.logout();
        }
        return Promise.reject(error);
    }
);

// App
import App from './App';

new Vue({
    el: '#app',
    render: (h) => h(App),
    components: {
        App
    }
});
